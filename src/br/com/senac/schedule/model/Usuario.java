
package br.com.senac.schedule.model;


public class Usuario {
    
    private String nome;
    private String senha;

    //CONSTRUTORES
    public Usuario() {
    }
    
    public Usuario(String nome, String senha) {
        this.nome = nome;
        this.senha = senha;
    }
    
    
    //GET e SET
    public String getNome() {
        return nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    
    
}
