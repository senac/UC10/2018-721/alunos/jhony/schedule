/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.schedule.view;

import br.com.senac.schedule.model.Pessoa;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jhony
 */
public class JFrameInicio extends JFrameModelo {

    private List<Pessoa> agenda;

    public JFrameInicio() {
        initComponents();
        this.agenda = new ArrayList<>();
        this.atualizarContador();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelInicial = new javax.swing.JPanel();
        jLabelImagemAgenda = new javax.swing.JLabel();
        jLabelQuantidadeContatos = new javax.swing.JLabel();
        jMenuBarBarraInicial = new javax.swing.JMenuBar();
        jMenuContatos = new javax.swing.JMenu();
        jMenuItemAdicionar = new javax.swing.JMenuItem();
        jMenuItemBuscar = new javax.swing.JMenuItem();
        jMenuSobre = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Agenda");

        jLabelImagemAgenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/senac/schedule/view/imagens/Mac-Address-Book-icon (1).png"))); // NOI18N

        jLabelQuantidadeContatos.setText("jLabel1");

        javax.swing.GroupLayout jPanelInicialLayout = new javax.swing.GroupLayout(jPanelInicial);
        jPanelInicial.setLayout(jPanelInicialLayout);
        jPanelInicialLayout.setHorizontalGroup(
            jPanelInicialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInicialLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelImagemAgenda)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInicialLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelQuantidadeContatos)
                .addContainerGap())
        );
        jPanelInicialLayout.setVerticalGroup(
            jPanelInicialLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInicialLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelQuantidadeContatos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabelImagemAgenda))
        );

        jMenuContatos.setText("Contatos");
        jMenuContatos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        jMenuItemAdicionar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/senac/schedule/view/imagens/add-icon.png"))); // NOI18N
        jMenuItemAdicionar.setText("Adicionar contato");
        jMenuItemAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAdicionarActionPerformed(evt);
            }
        });
        jMenuContatos.add(jMenuItemAdicionar);

        jMenuItemBuscar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/senac/schedule/view/imagens/Search-icon.png"))); // NOI18N
        jMenuItemBuscar.setText("Buscar contato");
        jMenuItemBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemBuscarActionPerformed(evt);
            }
        });
        jMenuContatos.add(jMenuItemBuscar);

        jMenuBarBarraInicial.add(jMenuContatos);

        jMenuSobre.setText("Ajuda");
        jMenuSobre.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jMenuSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuSobreActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Sobre");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenuSobre.add(jMenuItem1);

        jMenuBarBarraInicial.add(jMenuSobre);

        setJMenuBar(jMenuBarBarraInicial);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelInicial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelInicial, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuSobreActionPerformed

    }//GEN-LAST:event_jMenuSobreActionPerformed

    private void exibirSobre() {
        showMessageInformacao("Desenvolvido por Jhony Tavares\n"
                + "Turma 721\n"
                + "Tec. Informática\n"
                + "Versão da agenda: 1.0");
    }

    
    public void atualizarContador(){
        this.jLabelQuantidadeContatos.setText("Contatos:" + this.agenda.size());
    }
    
   

    private void jMenuItemBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemBuscarActionPerformed
        new JFrameBuscar(agenda, this);
    }//GEN-LAST:event_jMenuItemBuscarActionPerformed

    private void jMenuItemAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAdicionarActionPerformed
        new JFrameContato(agenda, this);
    }//GEN-LAST:event_jMenuItemAdicionarActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        this.exibirSobre();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameInicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelImagemAgenda;
    private javax.swing.JLabel jLabelQuantidadeContatos;
    private javax.swing.JMenuBar jMenuBarBarraInicial;
    private javax.swing.JMenu jMenuContatos;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItemAdicionar;
    private javax.swing.JMenuItem jMenuItemBuscar;
    private javax.swing.JMenu jMenuSobre;
    private javax.swing.JPanel jPanelInicial;
    // End of variables declaration//GEN-END:variables
}
