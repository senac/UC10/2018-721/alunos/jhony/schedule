package br.com.senac.schedule.view;

import br.com.senac.schedule.model.Pessoa;
import java.util.List;

public class JFrameContato extends JFrameModelo {

    private List<Pessoa> agenda;
    private Pessoa pessoa;
    private JFrameInicio jFrameInicio;

    public JFrameContato(List<Pessoa> agenda, JFrameInicio jFrameInicio) {
        initComponents();
        this.agenda = agenda;
        this.jFrameInicio = jFrameInicio;
        
    }
   
    public JFrameContato(List<Pessoa> agenda, JFrameInicio jFrameInicio, Pessoa pessoa){
        this(agenda, jFrameInicio) ;
        this.pessoa = pessoa;
        this.preencherFormulario();
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelContatos = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jLabelTelefone = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jFormattedTextFieldTelefone = new javax.swing.JFormattedTextField();
        jLabelImagemContato = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Adicionar contato");

        jLabelNome.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabelNome.setText("Nome:");

        jLabelTelefone.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabelTelefone.setText("Telefone:");

        jTextFieldNome.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N

        jButtonSalvar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonCancelar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        try {
            jFormattedTextFieldTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jFormattedTextFieldTelefone.setText("(   )      -    ");

        jLabelImagemContato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/senac/schedule/view/imagens/User-blue-icon.png"))); // NOI18N

        javax.swing.GroupLayout jPanelContatosLayout = new javax.swing.GroupLayout(jPanelContatos);
        jPanelContatos.setLayout(jPanelContatosLayout);
        jPanelContatosLayout.setHorizontalGroup(
            jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelContatosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelImagemContato)
                .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelContatosLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonCancelar)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonSalvar))
                    .addGroup(jPanelContatosLayout.createSequentialGroup()
                        .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNome, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelTelefone, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jFormattedTextFieldTelefone)
                            .addComponent(jTextFieldNome))))
                .addContainerGap())
        );
        jPanelContatosLayout.setVerticalGroup(
            jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContatosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelContatosLayout.createSequentialGroup()
                        .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelNome))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jFormattedTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelTelefone))
                        .addGap(29, 29, 29)
                        .addGroup(jPanelContatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvar)
                            .addComponent(jButtonCancelar)))
                    .addComponent(jLabelImagemContato))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelContatos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanelContatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
        String nome = jTextFieldNome.getText();
        String telefone = jFormattedTextFieldTelefone.getText();

        pessoa = new Pessoa(nome, telefone);
        this.agenda.add(pessoa);
        this.jFrameInicio.atualizarContador();
        showMessageInformacao("Contato salvo !!");
        this.limparJanela();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        this.dispose();
        this.jFrameInicio.atualizarContador();
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    
    public void limparJanela(){
        this.jTextFieldNome.setText("");
        this.jFormattedTextFieldTelefone.setText("");
        this.jTextFieldNome.requestFocus();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JFormattedTextField jFormattedTextFieldTelefone;
    private javax.swing.JLabel jLabelImagemContato;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelTelefone;
    private javax.swing.JPanel jPanelContatos;
    private javax.swing.JTextField jTextFieldNome;
    // End of variables declaration//GEN-END:variables

    private void preencherFormulario() {
        this.jTextFieldNome.setText(pessoa.getNome());
        this.jFormattedTextFieldTelefone.setText(pessoa.getTelefone());
    }
}
