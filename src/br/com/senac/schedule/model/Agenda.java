
package br.com.senac.schedule.model;

import java.util.ArrayList;
import java.util.List;


public class Agenda {
    
    private String nome;
    private String cor;
    private List<Pessoa> agenda;

    
    //Construtores
    public Agenda() {
        this.agenda = new ArrayList<>();
    }

    public Agenda(String nome) {
        this();
        this.nome = nome;
    }

    public Agenda(String nome, String cor) {
        this(nome);
        this.cor = cor;
    }

    
    //GET e SET
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return this.cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
    
    
    
    
    //MÉTODOS
    public void adicionarContato(Pessoa p){
        if(!this.agenda.contains(p)){
            this.agenda.add(p);
        }else{
            throw new RuntimeException("Contato ja existe na agenda !!");
        }
        
    }
    
    public void removerContato(String p){
        boolean achei = false;
        for (int i = 0; i < this.agenda.size(); i++){
            if (this.agenda.get(i).getNome().equalsIgnoreCase(nome)){
                this.agenda.remove(i);
                achei = true;
                break;
            }
            }
        if (!achei){
            throw new RuntimeException("Contato não existe na agenda.");
        
        }
    }
    
    public void removerContato(Pessoa p){
        if (this.agenda.contains(p)){
            this.agenda.remove(p);
        }else{
            throw new RuntimeException("Este contato não existe na agenda.");
        }
    }
    
    public Pessoa buscaContato(String nome){
        for (int i = 0; i < this.agenda.size(); i++){
            if (this.agenda.get(i).getNome().equals(nome)){
                return this.agenda.get(i);
            }
        }
        return null;
    }
    
    public int getQuantidadeContatos(){
        return this.agenda.size();
    }
    
    
    
    
    
    
}
